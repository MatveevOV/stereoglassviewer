#include "tcpsocket.h"

TcpSocket::TcpSocket(QObject *parent) : QObject(parent)
{
    m_pTcpSocket = new QTcpSocket(this);

    connect(m_pTcpSocket, &QTcpSocket::readyRead, this, &TcpSocket::slotReadyRead);
    connect(m_pTcpSocket, static_cast<void(QAbstractSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error),
            this, &TcpSocket::slotError);

    QSettings connectionSettings (QString ("settings"), QSettings::IniFormat);
    serverIp = connectionSettings.value("ServerAddress", "127.0.0.1").toString();
    serverPort = connectionSettings.value("ServerPort", 54256).toInt();

    m_pTcpSocket->connectToHost(serverIp,serverPort);
    m_nNextBlockSize = 0;

    m_isAndroid = false;
    dataHelper.setPlatfotm(m_isAndroid);


}

bool TcpSocket::isAndroid() const
{
    return m_isAndroid;
}

QString TcpSocket::port() const
{
    return QString::number(serverPort);
}

QString TcpSocket::address() const
{
    return serverIp;
}


void TcpSocket::slotReadyRead()
{
    QDataStream in(m_pTcpSocket);
    qDebug() << "client ready read" << m_nNextBlockSize <<m_pTcpSocket->bytesAvailable();
    in.setVersion(QDataStream::Qt_5_5);
    for (;;)
    {
        if (m_nNextBlockSize == 0) {
            if (m_pTcpSocket->bytesAvailable() < sizeof(quint64)) {
                break;
            }
            in >> m_nNextBlockSize;
        }

        if (m_pTcpSocket->bytesAvailable() < m_nNextBlockSize) {
            break;
        }

        quint8 type;
        in >> type;
        qDebug() << "type" << type;


        quint32 themeNameLength;
        quint32 picNameLength;
        QString themeName;
        QString picName;
        QByteArray bArray;
        QDataStream dStream(&bArray, QIODevice::ReadWrite);
        dStream.setVersion(QDataStream::Qt_5_5);

        switch (type) {
        case SEND_THEME:
        {
            qDebug() << "theme received";
            bArray = m_pTcpSocket->readAll();
            dStream >> themeNameLength;
            themeName = bArray.mid(sizeof(quint32), themeNameLength);
            qDebug() << themeName;
            dataHelper.openTheme(themeName);
            emit toTestMode();
        }
            break;
        case SEND_PICTURE:
        {
            qDebug() << "pic received";
            bArray = m_pTcpSocket->readAll();
            dStream >> picNameLength;
            picName = bArray.mid(sizeof(quint32), picNameLength);
            qDebug() << "received picture" << picName;
            dataHelper.setCurrentPictureName(picName);
            emit pictureReceived();
        }
            break;
        case TEST_END:
        {
            qDebug() << "pic end received";
            bArray = m_pTcpSocket->readAll();
            //завершение работы с темой, отправка сигнала на ui о прекращении теста
            emit testEndReceived();
        }
            break;
        default:
            break;
        }


        m_nNextBlockSize = 0;
    }

}

void TcpSocket::sendReadyToView()
{
    qDebug() << "client ready to view";
    sendRequest(READY_TO_VIEW);
}
//по клику на области
void TcpSocket::sendPictureViewed()
{
    qDebug() << "client viewed picture";
    sendRequest(PICTURE_VIEWED);
}

void TcpSocket::sendRequest(Command request)
{
    if (m_pTcpSocket->state() == QAbstractSocket::ConnectedState)
    {
        QByteArray block;
        QDataStream out(&block, QIODevice::WriteOnly);
        out.setVersion(QDataStream::Qt_5_5);
        out << quint64(0) << quint8(request);
        out.device()->seek(0);
        out << quint64(block.size() - sizeof(quint64));
        qDebug() << "write" << block << m_pTcpSocket->write(block);
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Нет соединения с сервером");
        msgBox.setWindowTitle("Помощник по обновлению");
        msgBox.exec();
    }

    m_nNextBlockSize = 0;


}

void TcpSocket::slotError(QAbstractSocket::SocketError err)
{
       m_pTcpSocket->close();
       QTimer::singleShot(5000, this, &TcpSocket::reconnectToServer);
}

void TcpSocket::reconnectToServer()
{
    if (m_pTcpSocket->state() == QAbstractSocket::UnconnectedState)
    {
        m_pTcpSocket->connectToHost(serverIp,serverPort);

    }
}

void TcpSocket::changeConnectionSettings(const QString &ip, const QString &port)
{
    serverIp = ip;
    serverPort = port.toInt();
    QSettings connectionSettings (QString ("settings"), QSettings::IniFormat);
    connectionSettings.setValue("ServerAddress", serverIp);
    connectionSettings.setValue("ServerPort", serverPort);

    m_pTcpSocket->disconnectFromHost();
    m_pTcpSocket->connectToHost(serverIp, serverPort);
}
