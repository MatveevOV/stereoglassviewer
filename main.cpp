#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "datahelper.h"
#include "tcpsocket.h"



int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QQmlApplicationEngine engine;

    //переменная платформа, для десктопа и андроида используются разные пути
    //к картинкам

    TcpSocket socket;



    engine.rootContext()->setContextProperty("socket", &socket);
    engine.rootContext()->setContextProperty("isAndroid", socket.isAndroid());
    engine.rootContext()->setContextProperty("filePrefix", "file:///");
    engine.rootContext()->setContextProperty("dataHelper", &socket.dataHelper);


    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
