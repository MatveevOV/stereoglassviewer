#ifndef DATAHELPER_H
#define DATAHELPER_H

#include <QObject>
#include <QDir>
#include <QDebug>
#include <QFile>

#define BACK               "Назад"


class DataHelper : public QObject
{
    Q_OBJECT
public:
    explicit DataHelper(QObject *parent = nullptr);
    void setPlatfotm(bool isAndroid);

signals:

public slots:
    void openTheme(QString theme);
    QString getLeftImage();
    QString getRightImage();
    QString getCurrentTheme() const;
    void setCurrentPictureName(QString pictureName);

private:
    struct Slide
    {
       QString id;
       QString leftImagePath;
       QString rightImagePath;
    };

    QString m_currentTheme;
    QString m_pictureName;
    int currentPicture;
    QMap<QString, Slide> slides;

    bool themeParse(QString dir);
    bool isAndroid;


};

#endif // DATAHELPER_H
