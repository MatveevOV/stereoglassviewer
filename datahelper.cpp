#include "datahelper.h"


DataHelper::DataHelper(QObject *parent) : QObject(parent)
{
}

void DataHelper::setPlatfotm(bool isAndroid)
{
    this->isAndroid = isAndroid;
}

QString DataHelper::getLeftImage()
{
    if(isAndroid)
        return slides.value(m_pictureName).leftImagePath;
    else
        return QFileInfo(slides.value(m_pictureName).leftImagePath).absoluteFilePath();

}

QString DataHelper::getRightImage()
{
    if(isAndroid)
        return slides.value(m_pictureName).rightImagePath;
    else
        return QFileInfo(slides.value(m_pictureName).rightImagePath).absoluteFilePath();
}

QString DataHelper::getCurrentTheme() const
{
    return m_currentTheme;
}

void DataHelper::setCurrentPictureName(QString pictureName)
{
    qDebug() << slides.size() << pictureName;
    if (slides.contains(pictureName))
        m_pictureName = pictureName;
    else
        m_pictureName = QString();

}



bool DataHelper::themeParse(QString dir)
{
    QDir themeDir(dir);
    slides.clear();
    if (themeDir.exists())
    {
        QStringList filters;
        filters << "*.jpg";
        QStringList fileList = themeDir.entryList(filters, QDir::Files, QDir::Name);
        for (int i = 0; i < fileList.length(); i+=2)
        {
            Slide slide;
            QString fileName = fileList.at(i);
            fileName.chop(6);//отрезать -L.jpg
            slide.id = fileName;
            slide.leftImagePath = QString(dir + "/" + fileName + "-L.jpg");
            slide.rightImagePath = QString(dir + "/" + fileName + "-R.jpg");
            slides.insert(slide.id,slide);

        }
        return true;
    }
    else
        return false;
}

void DataHelper::openTheme(QString theme)
{
    qDebug() << "the,e" << theme;
    m_currentTheme = theme;
    if (isAndroid)
        themeParse("assets:/Pictures/"+ m_currentTheme);
    else
        themeParse("./Pictures/"+ m_currentTheme);
    currentPicture = -1;

}
