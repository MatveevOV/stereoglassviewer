import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3




ApplicationWindow {
    id: window
    visible: true
    width: 1280
    height: 720
    title: qsTr("StereoGlass")

    property alias stack: stack
    property alias stereoComponent: stereoComponent


    property int mode: 0

    Connections{
    target: socket
    onToTestMode: { stack.push(stereoComponent) }

    }

    Shortcut {
        sequences: ["Esc", "Back"]
        enabled: stack.depth > 1
        onActivated: {
            stack.pop()
        }
    }

    StackView
    {
        id: stack
        anchors.fill:  parent
        initialItem:
        Item {
            Text {
                anchors.centerIn: parent
                font.pointSize: 14
            }
            Button {
                anchors.top: parent.top
                anchors.right: parent.right
                width: 200
                height: 50
                text: "Настройки подключения"
                onClicked: { settingsDialog.open()}

            }

            MouseArea
            {
                anchors.fill: parent
                onClicked: {
                    //stack.push(stereoComponent)

                }

            }

        }

    }

    Component
    {
        id: stereoComponent
        StereoView
        {
            id: stereoView
        }
    }

    Dialog {
        id: settingsDialog
        x: Math.round((window.width - width) / 2)
        y: Math.round(window.height / 6)
        width: Math.round(Math.min(window.width, window.height) / 3 * 2)
        modal: true
        focus: true
        title: "Настройки подключения"

        standardButtons: Dialog.Ok | Dialog.Cancel
        onAccepted: {
            socket.changeConnectionSettings(ipEdit.text,portEdit.text)
            settingsDialog.close()
        }
        onRejected: {
            settingsDialog.close()
        }

        contentItem: ColumnLayout {
            id: settingsColumn
            spacing: 20

            RowLayout {
                spacing: 10

                Label {
                    text: "IP Адрес:"
                }
                TextInput{
                    id: ipEdit
                    font.pointSize: 12
                    inputMask: "000.000.000.000"
                    text: socket.address()

                }

                Label {
                    text: "Порт:"
                }
                TextInput{
                    id: portEdit
                    font.pointSize: 12
                    text: socket.port()
                    validator: IntValidator{bottom: 0; top: 65535}

                }
            }
        }
    }

}

