import QtQuick 2.9
import QtQuick.Dialogs 1.1
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Item {

    property bool testEnd: false

    Connections{
    target: socket
    onPictureReceived: { setImage() }
    onTestEndReceived: { stack.pop()
 }

    }

    Rectangle{
        id: leftRect
        width: parent.width/2-2
        height: parent.height
        x:0
        y:0
        color: "red"
        clip: true
        Image{
            id: leftPic

        }
    }

    Rectangle{
        id: rightRect
        width: parent.width/2-2
        height: parent.height
        x: parent.width/2+2
        y:0
        color: "blue"
        clip: true

        Image{

            id: rightPic

        }

    }


    Text {
        id: righttext
        anchors.centerIn: rightRect
        font.pointSize: 14
        text: qsTr("Нажмите для начала тестирования")
    }


    Text {
        id: lefttext
        anchors.centerIn: leftRect
        font.pointSize: 14
        text: qsTr("Нажмите для начала тестирования")
    }

    MouseArea
    {
        anchors.fill: parent
        onClicked: {
            imageViewed()
            lefttext.visible = false
            righttext.visible = false


        }

    }
    Rectangle
    {
        id: leftMessageRect
        anchors.centerIn: leftRect
        visible: false
        color: "white"
        width: 250
        height: 50
        Text{
            text: "Тестирование завершено"
            anchors.centerIn: parent
            font.pointSize: 14
        }

    }

    Rectangle
    {
        id: rightMessageRect
        anchors.centerIn: rightRect
        visible: false
        color: "white"
        width: 250
        height: 50
        Text{
            text: "Тестирование завершено"
            anchors.centerIn: parent
            font.pointSize: 14
        }

    }


    function setImage()
    {


        var leftPath
        var rightPath
        var prefix

        if (isAndroid)
        {
            prefix = ""
            leftPath = dataHelper.getLeftImage();
            rightPath = dataHelper.getRightImage();
        }
        else
        {
            prefix = filePrefix
            leftPath = prefix + dataHelper.getLeftImage();
            rightPath = prefix + dataHelper.getRightImage();
        }
        if (leftPath !== prefix && rightPath !== prefix )
        {
            console.log(leftPath, rightPath)
            leftPic.source = leftPath;
            rightPic.source = rightPath;

            leftPic.fillMode = Image.PreserveAspectFit
            rightPic.fillMode = Image.PreserveAspectFit
            leftPic.height = leftRect.height
            rightPic.height = rightRect.height
            leftPic.x = leftRect.width/2 - leftPic.width/2
            rightPic.x = rightRect.width/2 - rightPic.width/2


        }

    }

    function imageViewed()
    {
        console.log(leftPic.width, rightPic.width)
        if(leftPic.width === 0 && rightPic.width === 0)
        {
            socket.sendReadyToView();
        }
        else
        {
            socket.sendPictureViewed();
        }
    }
}



