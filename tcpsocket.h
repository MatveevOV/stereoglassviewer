#ifndef TCPSOCKET_H
#define TCPSOCKET_H

#include <QObject>
#include <QTcpSocket>
#include <QFileDialog>
#include <QFile>
#include <QDir>
#include <QMessageBox>
#include <QEvent>
#include <QDialog>
#include <QSettings>
#include <QUuid>
#include <QNetworkProxy>
#include <QTimer>

#include "datahelper.h"


class TcpSocket: public QObject
{
    Q_OBJECT

public:
    explicit TcpSocket(QObject *parent = nullptr);
    enum Command{SEND_THEME,
                 SEND_PICTURE,
                 READY_TO_VIEW,
                 PICTURE_VIEWED,
                 TEST_END
                };


    bool isAndroid() const;
    DataHelper dataHelper;

private:
    QTcpSocket* m_pTcpSocket;
    qint64     m_nNextBlockSize;
    int serverPort;
    QString serverIp;
    bool m_isAndroid;


    void sendRequest(Command request);
    void slotReadyRead();
    void slotError(QAbstractSocket::SocketError err);
    void reconnectToServer();

public slots:
    void changeConnectionSettings(const QString &ip,const QString &port);
    void sendPictureViewed();
    void sendReadyToView();
    QString port() const;
    QString address() const;

signals:
    void toTestMode();
    void pictureReceived();
    void testEndReceived();
};

#endif // TCPSOCKET_H
